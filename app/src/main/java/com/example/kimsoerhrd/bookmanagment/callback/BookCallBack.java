package com.example.kimsoerhrd.bookmanagment.callback;

import com.example.kimsoerhrd.bookmanagment.entity.Book;

public interface BookCallBack  {

    void delete(Book book, int position);

    interface DialogCallBack{
        void getBook(Book book);
    }

    interface EditDialogCallBack{
        void editBook(Book book, int position);
    }
}
