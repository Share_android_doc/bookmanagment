package com.example.kimsoerhrd.bookmanagment;

import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.GridLayout;

import com.example.kimsoerhrd.bookmanagment.adapter.BookAdapter;
import com.example.kimsoerhrd.bookmanagment.callback.BookCallBack;
import com.example.kimsoerhrd.bookmanagment.dialog.AddDialogBook;
import com.example.kimsoerhrd.bookmanagment.dialog.EditDialogBook;
import com.example.kimsoerhrd.bookmanagment.entity.Book;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity  implements BookCallBack, BookCallBack.DialogCallBack, BookCallBack.EditDialogCallBack{

    List<Book> bookList;
    RecyclerView recyclerView;
    BookAdapter bookAdapter;
    Button btnDonate;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recyclerView = findViewById(R.id.rvBook);

        bookList = new ArrayList<>();
        bookAdapter = new BookAdapter(bookList,this);
        recyclerView.setLayoutManager(new GridLayoutManager(this,2));
        btnDonate = findViewById(R.id.btnDonate);
        btnDonate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AddDialogBook addDialogBook = new AddDialogBook();
                addDialogBook.show(getSupportFragmentManager(),"Add New");

            }
        });

        recyclerView.setAdapter(bookAdapter);
        getAllBook();
    }

    void getAllBook(){
        Drawable uri = getResources().getDrawable(R.drawable.book2);
        bookList.add(new Book( "Python: For Beginners A Crash Course Guide","Computer Science",5, 15, R.drawable.book2));
        bookList.add(new Book( "Python: For Beginners A Crash Course Guide","Computer Science",5, 15, R.drawable.book2));
        bookList.add(new Book( "Python: For Beginners A Crash Course Guide","Computer Science",5, 15,R.drawable.book2 ));
        bookList.add(new Book( "Python: For Beginners A Crash Course Guide","Computer Science",5, 15, R.drawable.book2));
        bookList.add(new Book( "Python: For Beginners A Crash Course Guide","Computer Science",5, 15, R.drawable.book2));
        bookList.add(new Book( "Python: For Beginners A Crash Course Guide","Computer Science",5, 15,R.drawable.book2 ));
        bookList.add(new Book( "Python: For Beginners A Crash Course Guide","Computer Science",5, 15, R.drawable.book2));
        bookList.add(new Book( "Python: For Beginners A Crash Course Guide","Computer Science",5, 15, R.drawable.book2));
        bookList.add(new Book( "Python: For Beginners A Crash Course Guide","Computer Science",5, 15, R.drawable.book2));
        bookList.add(new Book( "Python: For Beginners A Crash Course Guide","Computer Science",5, 15, R.drawable.book2));
    }

    @Override
    public void delete(Book book, int position) {
        bookList.remove(book);
        bookAdapter.notifyItemRemoved(position);

    }

    @Override
    public void getBook(Book book) {
        bookList.add(0, book);

        bookAdapter.notifyItemChanged(0);
        recyclerView.smoothScrollToPosition(0);
    }

    @Override
    public void editBook(Book book, int position) {
        //new data
        bookList.set(position,book);
        bookAdapter.notifyItemChanged(position);
        recyclerView.smoothScrollToPosition(position);
    }



}
