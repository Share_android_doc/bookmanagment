package com.example.kimsoerhrd.bookmanagment.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import com.example.kimsoerhrd.bookmanagment.R;
import com.example.kimsoerhrd.bookmanagment.callback.BookCallBack;
import com.example.kimsoerhrd.bookmanagment.entity.Book;

public class EditDialogBook extends DialogFragment {


    BookCallBack.EditDialogCallBack dialogCallBack;
    private Book book;
    private int position;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        dialogCallBack = (BookCallBack.EditDialogCallBack) context;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {

        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        View view = LayoutInflater.from(getActivity()).inflate(R.layout.add_book_layout, null);
        final EditDialogBook.MyViewHolder myViewHolder =  new EditDialogBook.MyViewHolder(view);
        //set data to from
        if(book!=null){
            myViewHolder.bookCategory.setText(book.getCat());
            myViewHolder.bookPrice.setText(""+book.getPrice());
            myViewHolder.bookTitle.setText(book.getTitle());
            myViewHolder.bookSize.setText(""+book.getSize());
        }
        builder.setView(view);

        final Book book = new Book();

        builder.setPositiveButton("Save", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                book.setTitle(myViewHolder.bookTitle.getText().toString());
                book.setPrice(Integer.parseInt(myViewHolder.bookPrice.getText().toString()));
                book.setCat(myViewHolder.bookCategory.getText().toString());
                book.setSize(Integer.parseInt(myViewHolder.bookSize.getText().toString()));

                dialogCallBack.editBook(book, position);
            }
        });
        return builder.create();
    }

    public void setData(Book book, int pos){
        this.book=book;
        this.position=pos;
    }

    class MyViewHolder{

        EditText bookTitle, bookCategory, bookSize, bookPrice;
        ImageView imageView;

        public MyViewHolder(View view) {
            bookTitle = view.findViewById(R.id.etBookTitle);
            bookCategory = view.findViewById(R.id.etBookCategory);
            bookPrice = view.findViewById(R.id.etBookPrice);
            bookSize = view.findViewById(R.id.etBookSize);
            imageView = view.findViewById(R.id.ivBookCover);

        }
    }

}
