package com.example.kimsoerhrd.bookmanagment.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;
import com.example.kimsoerhrd.bookmanagment.R;
import com.example.kimsoerhrd.bookmanagment.callback.BookCallBack;
import com.example.kimsoerhrd.bookmanagment.dialog.EditDialogBook;
import com.example.kimsoerhrd.bookmanagment.entity.Book;

import java.util.List;

public class BookAdapter extends RecyclerView.Adapter<BookAdapter.MyViewHold>{

    List<Book> bookList;
    Context context;
    LayoutInflater inflater;
    BookCallBack callBack;
    BookCallBack.EditDialogCallBack editDialogCallBack;
    FragmentManager.FragmentLifecycleCallbacks fragmentManager ;

    public BookAdapter(List<Book> bookList, Context context) {
        this.bookList = bookList;
        this.context = context;
        inflater = LayoutInflater.from(context);
        callBack = (BookCallBack) context;
        editDialogCallBack = (BookCallBack.EditDialogCallBack) context;

    }

    @NonNull

    @Override
    public MyViewHold onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = inflater.inflate(R.layout.book_list_layout, null);
        return new MyViewHold(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHold myViewHold, int i) {
        Book book = bookList.get(i);
        myViewHold.imageView.setImageResource(book.getImageUri());
        myViewHold.tvTitle.setText(book.getTitle());
        myViewHold.tvCat.setText(book.getCat());
        myViewHold.tvPrice.setText("Price : "+book.getPrice()+"$");
        myViewHold.tvSize.setText("Size : "+book.getSize()+"km");

    }

    @Override
    public int getItemCount() {
        return bookList.size();
    }

    class MyViewHold extends RecyclerView.ViewHolder {

        TextView tvTitle, tvCat, tvPrice, tvSize, tvShow;
        ImageView imageView;

        public MyViewHold(@NonNull View itemView) {
            super(itemView);
            tvTitle = itemView.findViewById(R.id.tvTitle);
            tvCat = itemView.findViewById(R.id.tvCategory);
            tvPrice = itemView.findViewById(R.id.tvPrice);
            tvSize = itemView.findViewById(R.id.tvSize);
            tvShow = itemView.findViewById(R.id.tvShow);
            imageView = itemView.findViewById(R.id.ivBook);

            tvShow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showPopupItem(v);
                }
            });
        }


        void showPopupItem(final View view){

            PopupMenu popupMenu = new PopupMenu(context,view);
            popupMenu.getMenuInflater().inflate(R.menu.pop_up, popupMenu.getMenu());
            popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(MenuItem item) {

                    switch (item.getItemId()){
                        case R.id.itemEdit:{

                            Book book = bookList.get(getAdapterPosition());
                            EditDialogBook editDialogBook=new EditDialogBook();
                            //pass adapter to dialog
                            editDialogBook.setData(book,getAdapterPosition());
                            editDialogBook.show(((AppCompatActivity) context).getSupportFragmentManager(),"edit Fragment");
                            //editDialogCallBack.editBook(book, getAdapterPosition());
                            return true;
                        }
                        case R.id.itemRemove:{
                            Book book = bookList.get(getAdapterPosition());
                            callBack.delete(book, getAdapterPosition());
                            return true;
                        }

                        case R.id.itemRead: {
                            return true;
                        }
                    }
                    return false;
                }
            });
            popupMenu.show();

        }

    }

}
