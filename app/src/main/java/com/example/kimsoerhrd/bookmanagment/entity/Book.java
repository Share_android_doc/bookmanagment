package com.example.kimsoerhrd.bookmanagment.entity;


import android.os.Parcel;
import android.os.Parcelable;

public class Book implements Parcelable {
    String title;
    String cat;
    int size;
    int price;
    int imageUri;

    public Book() {
    }

    public Book(String title, String cat, int size, int price, int imageUri) {
        this.title = title;
        this.cat = cat;
        this.size = size;
        this.price = price;
        this.imageUri = imageUri;
    }

    protected Book(Parcel in) {
        title = in.readString();
        cat = in.readString();
        size = in.readInt();
        price = in.readInt();
        imageUri = in.readInt();
    }

    public static final Creator<Book> CREATOR = new Creator<Book>() {
        @Override
        public Book createFromParcel(Parcel in) {
            return new Book(in);
        }

        @Override
        public Book[] newArray(int size) {
            return new Book[size];
        }
    };

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCat() {
        return cat;
    }

    public void setCat(String cat) {
        this.cat = cat;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getImageUri() {
        return imageUri;
    }

    public void setImageUri(int imageUri) {
        this.imageUri = imageUri;
    }

    @Override
    public String toString() {
        return "Book{" +
                "title='" + title + '\'' +
                ", cat='" + cat + '\'' +
                ", size=" + size +
                ", price=" + price +
                ", imageUri=" + imageUri +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(title);
        dest.writeString(cat);
        dest.writeInt(size);
        dest.writeInt(price);
        dest.writeInt(imageUri);
    }
}
